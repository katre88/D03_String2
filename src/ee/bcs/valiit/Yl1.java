package ee.bcs.valiit;

public class Yl1 {

    public static void main(String[] args) {
        String president1 = "Konstantin Päts";
        String president2 = "Lennart Meri";
        String president3 = "Arnold Rüütel";
        String president4 = "Toomas Hendrik Ilves";
        String president5 = "Kersti Kaljulaid";

        StringBuilder sb = new StringBuilder(String.format("%s, %s, %s, %s ja %s on Eesti presidendid", president1, president2, president3, president4, president5));
        System.out.println(sb);

        StringBuilder sb2 = new StringBuilder(president1).append(", ").append(president2).append(", ").append(president3).append(", ").append(president4).append(" ja ").append(president5).append(" on Eesti presidendid");

        System.out.println(sb2);

    }
}
